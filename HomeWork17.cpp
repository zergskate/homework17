﻿
#include <iostream>
#include <math.h>

class Vector
{
private:
    double X = 0;
    double Y = 0;
    double Z = 0;
public:
    Vector(double X1, double Y1, double Z1) : X(X1), Y(Y1), Z(Z1)
    {}
    void PrintValues()
    {
        std::cout << X << "." << Y << "." << Z << "\n";
    }
    void CalcLenght()
    {
        double L;
        L = sqrt((X * X) + (Y * Y) + (Z * Z));
        std::cout << "Vector Lenght = " << L;
    }
};

int main()
{
    Vector NewV(3, 4, 7);
    NewV.PrintValues();
    NewV.CalcLenght();
}

